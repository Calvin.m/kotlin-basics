package com.rave.kotlinbasic

fun calculateSum(firstNumber: Int = 10, secondNumber: Int = 5) {
    val result = firstNumber + secondNumber
    println("$firstNumber + $secondNumber = $result")
}

