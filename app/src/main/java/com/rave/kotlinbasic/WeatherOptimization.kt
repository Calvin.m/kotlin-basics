package com.rave.kotlinbasic

data class Weather(val city: String, val low: Int, val high: Int, val rain: Int)
fun printWeather(weather: Weather) {
    println("City: ${weather.city}")
    println("Low temperature: ${weather.low}, High temperature: ${weather.high}")
    println("Chance of rain: ${weather.rain}%")
    println()
}
