package com.rave.kotlinbasic

fun displayAlert(operatingSystem: String = "Unknown OS", emailId: String) =
    if (operatingSystem != "Unknown OS") {
        println("operatingSystem = $operatingSystem")
        println("emailId = $emailId")
        println("There's a new sign-in request on $operatingSystem for you Google Account $emailId")
    } else {
        println("There's a new sign-in request on $operatingSystem for you Google Account $emailId")
    }